# angular-task-manager

## Requirements
* Node.js >= 6.5.0
* npm >= 3.10.3

## Running the project
* `git clone https://vincent_stark@bitbucket.org/vincent_stark/angular-task-manager.git`
* `cd angular-task-manager`
* `npm install`
* `npm run-script "build.and.start"`
* Project will be available at http://localhost:9000/
