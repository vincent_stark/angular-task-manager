export class TaskInterface {
  id: string;
  name: string;
  description: string;
  estimate: number;
  state: string;
}
