/**
 * This barrel file provides the exports for the shared resources (services, components).
 */
export * from './services/index';
export * from './component/index';
export * from './config/env.config';
export * from './interfaces/index';
export * from './pipes/index';
