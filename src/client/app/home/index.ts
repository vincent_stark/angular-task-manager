/**
 * This barrel file provides the export for the lazy loaded HomeComponent.
 */
export * from './state/state.component';
export * from './task/task.component';
export * from './taskform/taskForm.component';
